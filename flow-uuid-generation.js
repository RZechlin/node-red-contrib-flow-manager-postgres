const { v4: uuidv4 } = require('uuid');


function generateId() {
    return uuidv4();
}

function generateId2() {
    var bytes = [];
    for (var i=0;i<8;i++) {
        bytes.push(Math.round(0xff*Math.random()).toString(16).padStart(2,'0'));
    }
    return bytes.join("");
}

console.log(generateId())