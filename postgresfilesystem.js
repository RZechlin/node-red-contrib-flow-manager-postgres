const {Pool} = require('pg')
// import uuid4 from "uuid4";

//let log = require("@node-red/util").log;

//using json instead of jsonb because I will not be querying inside the json, just returning it
const databaseColumns = `(
    id varchar PRIMARY KEY,
    data json,
    title varchar,
    description varchar,
    owner varchar,
    owner_id integer,
    workflow_type varchar,
    file_name varchar
  )`

const libraryColumns = `(
    type varchar(9),
    filepath varchar,
    filename varchar,
    meta json,
    file varchar
  )`

let settings;

let postgresfilesystem = {
    //pool,
    schema: "public",
    init: async function () {
        let db_host = process.env.DB_HOST;
        let db_user = process.env.DB_USER;
        let db_password = process.env.DB_PASSWORD;
        let server_name = process.env.DB_SERVER;
        const connectionString = "postgres://" + db_user + ":" + db_password + "@" + server_name + ":5432/" + db_host;
        // Connection String for Dev outside of Docker
        // const connectionString = "postgres://postgres:DIB-central@localhost:5432/workflow-designer-node-red";
        // settings = _settings;

        // if (!settings.postgresURI) {
        //     console.log("No settings.postgresURI Found!");
        // } else {
        try {
            this.pool = new Pool({
                'connectionString': connectionString,
                // 'connectionString': "postgres://postgres:DIB-central@db-workflow-designer/workflow-designer",
                //max: 10, //default
                //idleTimeoutMillis: 10000, //default   //(10 seconds)
                //connectionTimeoutMillis: 0, //default
            });
            // if (settings.postgresSchema) {
            //     this.schema = settings.postgresSchema;
            // }

            await this.pool.query(`CREATE TABLE IF NOT EXISTS ${this.schema}.workflow ${databaseColumns}`);

            if ((await this.pool.query(`SELECT * FROM ${this.schema}.workflow`)).rowCount === 0) {
                await this.pool.query(`INSERT INTO ${this.schema}.workflow (id, data) VALUES ('-','{}')`);
            }
            await this.pool.query(`CREATE TABLE IF NOT EXISTS ${this.schema}.noderedlibrary ${libraryColumns}`);


        } catch (err) {
            console.log("Failed to connect to postgres DB", err);
        }
        // }

    },

    getAllFlows: async function () {
        return (await this.pool.query(`SELECT * FROM ${this.schema}.workflow`)).rows;
    },

    getFlow: async function () {
        return
    },

    saveFlow: async function (flowId, flow, title) {
        // console.log(flow)
        return this.pool.query(`INSERT INTO ${this.schema}.workflow (id, data, title, workflow_type) 
                                VALUES ($2, $1, $3, $4) ON CONFLICT (id) DO UPDATE SET data = $1, title = $3`,
            [JSON.stringify(flow), flowId, title, "node-red"],
            function (err, result) {
                if (err) throw err
                // console.log(`insert ${result.id} success`)
                // console.log(result)
            });
    },
    saveFlows: async function (flows) {
        // return this.pgSave('flows',JSON.stringify(flows));
        return this.pgSave('flows', flows);
    },
    getCredentials: async function () {
        return this.pgGet('credentials');
    },
    saveCredentials: async function (credentials) {
        return this.pgSave('credentials', JSON.stringify(credentials));
    },
    getSettings: async function () {
        return this.pgGet('settings');
    },
    saveSettings: async function (settings) {
        return this.pgSave('settings', JSON.stringify(settings));
    },

    pgGet: async function (get) {
        return (await this.pool.query(`SELECT ${get} FROM ${this.schema}.workflow`)).rows[0][get];
    },
    pgSave: async function (save, flow) {
        return this.pool.query(`UPDATE ${this.schema}.workflow SET ${save} = $1`, [flow]);
    },

    //getSessions: sessions.getSessions,
    //saveSessions: sessions.saveSessions,
    //projects: projects

    getLibraryEntry: async function (type, path) {
        //console.log('getLibraryEntry',type,path);
        if ((type !== "flows") && (type !== "functions")) {
            return; //throw new err;
        }
        let toReturn = [];
        let foldersPushed = new Set();
        const sqlRes = (await this.pool.query(`SELECT * FROM ${this.schema}.noderedlibrary WHERE type=$1`, [type])).rows;
        for (let row of sqlRes) {
            if (path == `${row.filepath}${row.filename}`) {
                return row.file;
            } else if (path == row.filepath) {
                toReturn.push({'fn': row.filename});
            } else if (row.filepath.startsWith(path)) {
                const folderName = row.filepath.replace(path, '').split('/')[0];
                if (!(foldersPushed.has(folderName))) {
                    foldersPushed.add(folderName);
                    toReturn.push(folderName);
                }
            }
        }
        return toReturn;
    },

    saveLibraryEntry: async function (type, path, meta, body) {
        //console.log('saveLibraryEntry',type,path,meta,body);
        if ((type !== "flows") && (type !== "functions")) {
            return; //throw new err;
        }
        let splitPath = path.split('/');
        const filename = splitPath[splitPath.length - 1];
        const filepath = path.replace(filename, '');
        return await this.pool.query(`INSERT INTO ${this.schema}.noderedlibrary (type, filepath, filename, meta, file) VALUES ($1,$2,$3,$4,$5)`, [type, filepath, filename, meta, body]);
    }

};

module.exports = postgresfilesystem;